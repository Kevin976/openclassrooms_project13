from django.urls import reverse
from django.test import Client
from pytest_django.asserts import assertTemplateUsed


def test_index_view():
    client = Client()
    path = reverse("index")
    response = client.get(path)
    content = response.content.decode()
    expected_content = '<title>Holiday Homes</title>\n' \
                       '<h1>Welcome to Holiday Homes</h1>\n' \
                       '<div><a href="/profiles/">Profiles</a></div>\n' \
                       '<div><a href="/lettings/">Lettings</a></div>'

    assert content == expected_content
    assert response.status_code == 200
    assertTemplateUsed(response, "index.html")
